import { Component, Input } from "@angular/core";

@Component({
    selector: 'ap-contato',
    templateUrl:'contato.component.html'

})

export class ContatoComponent{
    @Input() nome='';
    @Input() email='';
    @Input() telefone='';
}