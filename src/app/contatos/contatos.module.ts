import { NgModule } from "@angular/core";
import { ContatoComponent } from "./contato/contato.component";

@NgModule({
    declarations: [ContatoComponent],
    exports: [ContatoComponent]
})

export class ContatosModule{}